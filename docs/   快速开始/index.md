# 快速开始

## 中心ArkID
如果希望快速的了解系统的基本使用，可以访问 [https://saas.akid.cc](https://saas.arkid.cc)

注册后，创建自己的租户，即可使用系统的大部分功能

!!! 提示
    如果希望体验**超级管理员**，**安装配置插件**等，推荐使用 **[私有化部署](#私有化部署)** 的方式

## 私有化部署

[通过ArkOS部署(推荐)](./%20私有化部署/通过ArkOS部署/){.md-button}
[通过docker部署](./%20私有化部署/通过docker部署/){.md-button}
[通过k8s部署](./%20私有化部署/通过k8s部署/){.md-button}