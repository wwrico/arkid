# 多语言包插件：简体中文



## 抽象方法实现
* [language_type](#extension_root.com_longgui_language_zh.TranslationZhExtension.language_type)
* [language_data](#extension_root.com_longgui_sms_aliyun.TranslationZhExtension.language_data)

## 代码

::: extension_root.com_longgui_language_zh.TranslationZhExtension
    rendering:
        show_source: true